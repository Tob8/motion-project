import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { fetchLogin } from '../../store/actions/loginActions.jsx';
import { connect } from 'react-redux'

import './index.css';

import logoMotionWhite from '../../assets/images/logo_white.png';
import logoMotionSmall from '../../assets/images/logo.png';
import logoApple from '../../assets/svgs/apple.svg'
import logoGoogle from '../../assets/svgs/google.svg'
import iconTwitter from '../../assets/svgs/twitter_icon.svg'
import iconFacebook from '../../assets/svgs/facebook_icon.svg'
import iconInstagram from '../../assets/svgs/instagram_icon.svg'
import iconUseravatar from '../../assets/svgs/avatar.svg'
import iconPassword from '../../assets/svgs/password.svg'
import iconHeart from '../../assets/svgs/heart.svg'
import iconFriends from '../../assets/svgs/icon-friends.svg'
import iconJennifer from '../../assets/images/users/jennifer.png'
import iconPatricia from '../../assets/images/users/patricia.png'
import iconAlber from '../../assets/images/users/alber.png'
import iconMenu from '../../assets/svgs/menu.svg'
import iconNotification from '../../assets/svgs/notification_bell.svg'
import iconPosts from '../../assets/svgs/posts_logo.svg'
import iconSearch from '../../assets/svgs/search_icon.svg'
import iconSend from '../../assets/svgs/send_button.svg'
import iconShare from '../../assets/svgs/share.svg'
import feedPic1 from '../../assets/images/feedPics/Image.png'
import feedPic2 from '../../assets/images/feedPics/Image2.png'
import feedPic3 from '../../assets/images/feedPics/Image3.png'
import feedPic4 from '../../assets/images/feedPics/Image4.png'
import feedPicLarge from '../../assets/images/feedPics/large.png'

// function HomePage() {
const LoginPage = (props) => {

  const [email, setEmail] = useState('please.press@signin-to-see.more');

  const [password, setPassword] = useState('testingtests');

  const handleChangePassword = (event) => {
    setPassword(event.currentTarget.value)
  }

  const login = async (event) => {
    event.preventDefault();
    const result = await props.dispatch(fetchLogin(email, password));
    console.log(result);
    props.history.push('/posts');
  }

  return (

    <div className="loginPage">

      <div className="leftSide">
        <div className="topSection">

          <div className="logoPart">
            <img className="logoPicture" src={logoMotionWhite} alt="logo"></img>
            <h1>Motion</h1>
            <p className="p16">Connect with friends and the world around you with Motion.</p>
          </div>

          <div className="buttonsPart">
            {/* <button className="logoButtons">
              <img src={logoApple} alt="" />
            </button> */}

            <a href="https://www.apple.com/chde/ios/app-store/" target="_blank" rel="noopener noreferer" className="logoButtons" ><img src={logoApple} alt="Apple Store Logo"></img></a>
            <a href="https://play.google.com/store/apps" target="_blank" rel="noopener noreferer" className="logoButtons" ><img src={logoGoogle} alt="Google Play Logo"></img></a>

          </div>

        </div>


        <div className="logoIcons">
          <a href="https://twitter.com" target="_blank" rel="noopener noreferer" ><img className="icons" src={iconTwitter} alt="Twitter Logo"></img></a>
          <a href="https://facebook.com" target="_blank" rel="noopener noreferer" ><img className="icons" src={iconFacebook} alt="Facebook Logo"></img></a>
          <a href="https://instagram.com" target="_blank" rel="noopener noreferer" ><img className="icons" src={iconInstagram} alt="Instagram Logo"></img></a>
        </div>

        <footer className="copywrite">
          <p className="p12">&copy; Motion 2020. All rights reserved.</p>
        </footer>

      </div>



      <div className="rightSide">

        <header>
          <div className="headerContent">
            <p className="p14">Don't have an account?</p>
            <button>Sign up</button>
          </div>
        </header>

        <div className="signIn">
          <h1 className="loginTitle">Sign In</h1>

          <form className="loginForm" onSubmit={login}>

            <div class="inputContainer">
              <input
                class="userName"
                type="email"
                placeholder="Username"
                value={email}
                onChange={(event) => setEmail(event.currentTarget.value)} />
            </div>

            <div class="inputContainer">
              <input
                class="passWord"
                type="password"
                placeholder="Password"
                value={password}
                onChange={handleChangePassword} />
            </div>

            <div className="loginButton">
              <button className="logBtn" type="submit">Sign In</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  );
}

export default connect()(LoginPage);
