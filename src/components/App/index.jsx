import React from 'react';
import { HashRouter, Switch, Route, Link } from "react-router-dom";
import { Provider } from 'react-redux'
import store from '../../store'
import LoginPage from '../Login';
import FeedPage from '../Posts';


// function App() {

const App = (props) => {

  return (
    <Provider store={store}>
      <HashRouter>
        <Switch>
          <Route exact path="/" component={LoginPage} />
          <Route exact path="/posts" component={FeedPage} />
        </Switch>
      </HashRouter>
    </Provider>
    // <>
    //   <Router>
    //     <Switch>
    //       <Route path="/home">
    //         <HomePage />
    //       </Route>
    //       <Route path="/feed">
    //         <FeedPage />
    //       </Route>
    //     </Switch>
    //   </Router>

    // </>
  );
}

export default App;
