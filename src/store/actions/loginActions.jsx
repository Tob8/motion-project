export const fetchLogin = (email, password) => async (dispatch, getState) => {

  const body = JSON.stringify({
    email: email,
    password: password,
  })

  const headers = {
    'Content-Type': 'application/json'
  }

  const config = {
    method: 'POST',
    body: body,
    headers: headers,
  }

  console.log('here is my login action');
  let data;
  try {
    const response = await fetch('https://motion.propulsion-home.ch/backend/api/auth/token/', config);
    data = await response.json()
  }
  catch (error) {
    console.log('Error', error)
  }
  console.log(data.access);
  localStorage.setItem('accessToken', data.access);
  dispatch({
    type: 'fetchLogin',
    payload: data.access,
  });
};