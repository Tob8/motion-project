import React from 'react';
import './App.css';
import logoMotionWhite from './assets/images/logo_white.png';
import logoMotionSmall from './assets/images/logo.png';
import logoApple from './assets/svgs/apple.svg'
import logoGoogle from './assets/svgs/google.svg'
import iconTwitter from './assets/svgs/twitter_icon.svg'
import iconFacebook from './assets/svgs/facebook_icon.svg'
import iconInstagram from './assets/svgs/instagram_icon.svg'
import iconUseravatar from './assets/svgs/avatar.svg'
import iconPassword from './assets/svgs/password.svg'
import iconHeart from './assets/svgs/heart.svg' 
import iconFriends from './assets/svgs/icon-friends.svg'
import iconJennifer from './assets/images/users/jennifer.png'
import iconPatricia from './assets/images/users/patricia.png'
import iconAlber from './assets/images/users/alber.png'
import iconMenu from './assets/svgs/menu.svg'
import iconNotification from './assets/svgs/notification_bell.svg'
import iconPosts from './assets/svgs/posts_logo.svg'
import iconSearch from './assets/svgs/search_icon.svg'
import iconSend from './assets/svgs/send_button.svg'
import iconShare from './assets/svgs/share.svg'
import feedPic1 from './assets/images/feedPics/Image.png'
import feedPic2 from './assets/images/feedPics/Image2.png'
import feedPic3 from './assets/images/feedPics/Image3.png'
import feedPic4 from './assets/images/feedPics/Image4.png'
import feedPicLarge from './assets/images/feedPics/large.png'

function App() {

  return (
    <>


    <div className="feedPage">

      <div className="feedHeader">
        <div className="feedHeader_left">
          
          <div className="feedLogo">
            <img src={logoMotionSmall} alt="" />
            <h2 className="h222">Motion</h2>
          </div>

          <div className="feedPosts">
            <img src={iconPosts} alt="" />
            <p>Posts</p>
          </div>

          <div className="feedFriends">
            <img src={iconFriends} alt="" />
            <p>Find Friends</p>
          </div>

          {/* <div className="postFriends">
            <img src={iconPosts} alt="" />
            <p>Posts</p>
            <img src={iconFriends} alt="" />
            <p>Find Friends</p>
          </div> */}

        </div>


      <div className="feedHeader_right">

        <div className="infoMenu">
          <img src={iconNotification} alt="" />
          <img src={iconJennifer} alt="" />
          <img src={iconMenu} alt="" />
        </div>

      </div>

      </div>

      <div className="feedTop">
        <div className="search">
          <img src={iconSearch} alt="" />
          <p className="colorGray">Find Friends</p>
        </div>
        <div className="likesFriendsFollow">
          <div className="likesFriendsFollow_likes"><p>Liked</p></div>
          <div className="likesFriendsFollow_friends"><p className="colorGray">Friends</p></div>
          <div className="likesFriendsFollow_follow"><p className="colorGray">Follow</p></div>
        </div>
      </div> 

      <div className="topDividerLine"></div>

      <div className="feedContainer">

          <div className="newPost">
            <div className="newPost_left">
              <img className="newPost_picture" src={iconJennifer} alt="" />
              <p className="colorGray">What's on your mind, Jennifer?</p>
            </div>
            <div className="newPost_right">
              <div className="newPost_button">
                <img  src={iconSend} alt="" />
              </div>
            </div>
          </div>

        <div className="post">
          <div className="post_header">
              <div className="post_header_left">
                <img src={iconJennifer} alt="" />
                <div className="post_header_nameUpdate">
                  <p className="p14">Jennifer Smith</p>
                  <p className="p12 colorGray">Just now</p>
                </div>
              </div>
              <div className="post_header_right">
                <div className="iconMenu">
                  <img src={iconMenu} alt="" />
                </div>
              </div>
          </div>
          <div className="post_text">
            <p className="p16">Lorem ipsum dolor sit amet, vim ut quas volumus probatus, has tantas laudem iracundia et, ad per utamur ceteros apeirian…</p>
          </div>
          <div className="post_pictures">
            <img src={feedPic1} alt="" />
            <img src={feedPic2} alt="" />
            <img src={feedPic3} alt="" />
            <img src={feedPic4} alt="" />
          </div>
          <div className="post_footer">
            <div className="post_footer_likeShare">
              
              <div className="post_footer_like">
                <img src={iconHeart} alt="" />
                <p className="p14">Like</p>
              </div>

              <div className="post_footer_like">
                <img src={iconShare} alt="" />
                <p className="p14">Share</p>
              </div>

            </div>
            <div className="post_footer_likeCounter">
              <p className="p12 colorGray">2 Likes</p>
            </div>
          </div>
        </div>

        <div className="post">
          <div className="post_header">
              <div className="post_header_left">
                <img src={iconPatricia} alt="" />
                <div className="post_header_nameUpdate">
                  <p className="p14">Patricia Jindal</p>
                  <p className="p12 colorGray">6h ago</p>
                </div>
              </div>
              <div className="post_header_right">
                <div className="iconMenu">
                  <img src={iconMenu} alt="" />
                </div>
              </div>
          </div>
          <div className="post_text">
            <p className="p16">Lorem ipsum dolor sit amet, vim ut quas volumus probatus, has tantas laudem iracundia et, ad per utamur ceteros apeirian…</p>
          </div>
          {/* <div className="post_pictures">
            <img src={""} alt="" />
            <img src={""} alt="" />
            <img src={""} alt="" />
            <img src={""} alt="" />
          </div> */}
          <div className="post_footer">
            <div className="post_footer_likeShare">
              
              <div className="post_footer_like">
                <img src={iconHeart} alt="" />
                <p className="p14">Like</p>
              </div>

              <div className="post_footer_like">
                <img src={iconShare} alt="" />
                <p className="p14">Share</p>
              </div>

            </div>
            <div className="post_footer_likeCounter">
              <p className="p12 colorGray">3 Likes</p>
            </div>
          </div>
        </div>
        


        <div className="repost">
          <div className="repost_header">
              <div className="post_header_left">
                <img src={iconAlber} alt="" />
                <div className="post_header_nameUpdate">
                  <p className="p14">Alber Lawrence</p>
                  <p className="p12 colorGray">June 20</p>
                </div>
              </div>
              <div className="repost_header_right">
                <p className="p12 colorGray">shared a post</p>
              </div>
          </div>
          <div className="post_text">
            <p className="p16">Lorem ipsum dolor sit amet, vim ut quas volumus probatus, has tantas laudem iracundia et, ad per utamur ceteros apeirian…</p>
          </div>

          <div>


          <div className="repostContainer">
            <div className="post_header">
              <div className="post_header_left">
                <img src={iconPatricia} alt="" />
                <div className="post_header_nameUpdate">
                  <p className="p14">Patricia Jindal</p>
                  <p className="p12 colorGray">6h ago</p>
                </div>
              </div>

              {/* <div className="post_header_right">
                <div className="iconMenu">
                  <img src={iconMenu} alt="" />
                </div>
              </div> */}

            </div>
          <div className="post_text">
            <p className="p16">En krasse Demotext, damit ich au de Unterschied gseh, has tantas laudem iracundia et, ad per utamur ceteros apeirian…</p>
          </div>
          <div className="post_pictures">
            <img src={feedPicLarge} alt="" />
          </div>
        </div>


          </div>

          <div className="post_footer">
            <div className="post_footer_likeShare">
              
              <div className="post_footer_like">
                <img src={iconHeart} alt="" />
                <p className="p14">Like</p>
              </div>

              <div className="post_footer_like">
                <img src={iconShare} alt="" />
                <p className="p14">Share</p>
              </div>

            </div>
            <div className="post_footer_likeCounter">
              <p className="p12 colorGray">3 Likes</p>
            </div>
          </div>
        </div>



      </div>

    </div>





    <div className="loginPage">

      <div className="leftSide">
        <div className="topSection">

          <div className="logoPart">
            <img className="logoPicture" src={logoMotionWhite} alt="logo"></img>
            <h1>Motion</h1>
            <p className="p16">Connect with friends and the world around you with Motion.</p>
          </div>

          <div className="buttonsPart">
            <button className="logoButtons">
              <img src={logoApple} alt=""/>
            </button>
            <button className="logoButtons">
              <img  src={logoGoogle} alt=""/>
            </button>
          </div>

        </div>


          <div className="logoIcons">
            <img className="icons" src={iconTwitter} alt="Twitter Logo"></img>
            <img className="icons" src={iconFacebook} alt="Facebook Logo"></img>
            <img className="icons" src={iconInstagram} alt="Instagram Logo"></img>
          </div>

          <footer className="copywrite">
            <p className="p12">&copy; Motion 2020. All rights reserved.</p>
          </footer>

      </div>



      <div className="rightSide">

        <header>
          <div className="headerContent">
            <p className="p14">Don't have an account?</p>
            <button>Sign up</button>
          </div>
        </header>

        <div className="signIn">
          <h1 className="loginTitle">Sign In</h1>
          
          <div className="loginForm">

            <div class="inputContainer">
              {/* <img className="inputIcon " src={iconUseravatar} alt="User"></img> */}
              <input class="userName" type="text" placeholder="Username" />
            </div>

            <div class="inputContainer">
              {/* <img className="inputIcon" src={iconPassword} alt="Password"></img> */}
              <input class="passWord" type="password" placeholder="Password" />
            </div>

          </div>

        </div>

        <div className="loginButton">
          <button className="logBtn">Sign In</button>
        </div>

      </div>

    </div>

    </>
  );
}

export default App;
